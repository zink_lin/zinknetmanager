//
//  ZinkNetManager.m
//  ZinkNetManager
//
//  Created by mac on 14-7-22.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ZinkNetManager.h"
static ZinkNetManager * networkManager;

@implementation ZinkNetManager{
    NSMutableDictionary * _dicProgressInfo;
}

+(id)shareNetworkManager{
    if (networkManager) {
        return networkManager;
    }else{
        networkManager = [[ZinkNetManager alloc] init];
        return networkManager;
    }
}

-(id)init{
    self = [super init];
    if (self) {
        _queue = [[ASINetworkQueue alloc] init];
        _dicProgressInfo = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSString *)urlEncode:(NSString *)string{
    ASIFormDataRequest *formDataRequest = [ASIFormDataRequest requestWithURL:nil];
    return [formDataRequest encodeURL:string];
}

-(void)getWithUrl:(NSString *)urlStr andIdentiffier:(NSString *)identiffier andShowProgress:(BOOL)isShow{
    _isRequesting = YES;
    NSURL *requestUrl;
    
    if ([identiffier isEqualToString:zinkIdTest] || [identiffier hasSuffix:@"Test"]) {
        requestUrl = [NSURL URLWithString:urlStr];
    }else{
        requestUrl = [NSURL URLWithString:[zinkServerDomain stringByAppendingString:urlStr]];
    }
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:requestUrl];
    
    
    [request setRequestMethod:@"GET"];
    request.username = identiffier;
    [request setDelegate:self];
    
    if (isShow) {
        [request setDownloadProgressDelegate:self];
        [request startAsynchronous];
    }else{
        [_queue addOperation:request];
        [_queue go];
    }
}

-(void)postWithUrlstr:(NSString *)urlStr andDicInfo:(NSDictionary *)dicInfo andIdentiffier:(NSString *)identiffier andShowProgress:(BOOL)isShow{
    _isRequesting = YES;
    NSURL * requestUrl;
    
    if ([identiffier isEqualToString:zinkIdTest] || [identiffier hasSuffix:@"Test"]) {
        requestUrl = [NSURL URLWithString:urlStr];
    }else{
        requestUrl = [NSURL URLWithString:[zinkServerDomain stringByAppendingString:urlStr]];
    }
    
    
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:requestUrl];
    
    request.username = identiffier;
    request.delegate = self;
    NSArray * allKeys = [dicInfo allKeys];
    for (NSString * key in allKeys) {
        NSString * value = [dicInfo objectForKey:key];
        [request addPostValue:value forKey:key];
    }
    
    if (isShow) {
        [request setDownloadProgressDelegate:self];
        [request startAsynchronous];
    }else{
        [_queue addOperation:request];
        [_queue go];
    }
}

-(void)uploadFileWithUrl:(NSString *)urlStr andDicInfo:(NSDictionary *)dicInfo andFilePath:(NSString *)filePath andIdentiffier:(NSString *)identiffier{
    _isRequesting = YES;
    
    NSURL * requestUrl;
    
    if ([identiffier isEqualToString:zinkIdTest] || [identiffier hasSuffix:@"Test"]) {
        requestUrl = [NSURL URLWithString:urlStr];
    }else{
        requestUrl = [NSURL URLWithString:[zinkServerDomain stringByAppendingString:urlStr]];
    }
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:requestUrl];
    
    request.username = identiffier;
    request.delegate = self;
    NSArray * allKeys = [dicInfo allKeys];
    for (NSString * key in allKeys) {
        NSString * value = [dicInfo objectForKey:key];
        [request addPostValue:value forKey:key];
    }
    [request setFile:filePath forKey:@"file"];
    
    
    [_queue addOperation:request];
    [_queue go];
}

-(void)startHubWithView:(UIView *)contentView{
    //初始化并显示加载框
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:contentView animated:YES];
    hud.tag = zinkHudTagStop;
    [hud addTapAction];
    self.contentView = contentView;
    hud.delegate = self;
    [hud setLabelText:@"取消加载"];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}
-(void)startHubWithView:(UIView *)contentView andContent:(NSString *)strContent{
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:contentView animated:YES];
    hud.tag = zinkHudTagOther;
    [hud addTapAction];
    self.contentView = contentView;
    hud.delegate = self;
    [hud setLabelText:strContent];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}
-(void)stopHub{
    if (self.contentView) {
        [MBProgressHUD hideHUDForView:self.contentView animated:YES];
        self.contentView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

-(void)stopAllRequest{
    _isRequesting = NO;
    [_queue cancelAllOperations];
    
    [self stopHub];
}

#pragma mark MBProgressHUDDelegate
//提示框被点击时
-(void)hudWasTap:(MBProgressHUD *)hud{
    if (hud.tag == zinkHudTagStop) {
        [self stopAllRequest];
    }else if (hud.tag == zinkHudTagOther){
        
    }
    
}

#pragma mark ASIHttpRequestDelegate
//ASIHttpRequest数据请求完成
-(void)requestFinished:(ASIHTTPRequest *)request{
    id result;
    
    if ([request.username hasPrefix:@"str"]){
        NSData * data = request.responseData;
        result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [self.delegate zinkNetFinish:self withResult:result andIdentiffier:request.username];
    }else if ([request.username hasPrefix:@"file"]){
        NSData * data = request.responseData;
        
        [self.delegate zinkNetFinish:self withFile:data andIdentiffier:request.username];
    }else{
        NSData * data = request.responseData;
        
        if (ParserMethod == JSONParser) {
            result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        }else if (ParserMethod == XMLParser){
            SHXMLParser * parser = [[SHXMLParser alloc] init];
            result = [parser parseData:data];
        }
        
        
        [self.delegate zinkNetFinish:self withResult:result andIdentiffier:request.username];
    }
    
    if (_queue.requestsCount == 0) {
        _isRequesting = NO;
    }
}
//ASIHttpRequest数据请求失败
-(void)requestFailed:(ASIHTTPRequest *)request{
    if (self.contentView) {
        [MBProgressHUD hideHUDForView:self.contentView animated:YES];
        self.contentView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError * error = [[NSError alloc] initWithDomain:@"取消" code:ZErrorCode_Cancel userInfo:@{@"info": @"取消"}];
        
        [self.delegate zinkNet:self failWithError:error];
    }
    
    if (_isRequesting) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"加载失败" message:@"请检查网络设置" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
        [self stopAllRequest];
        
        NSError * error = [[NSError alloc] initWithDomain:@"无网络" code:ZErrorCode_NoNetwork userInfo:@{@"info": @"无网络"}];
        
        [self.delegate zinkNet:self failWithError:error];
    }
    
    if (_queue.requestsCount == 0) {
        _isRequesting = NO;
    }
}

#pragma mark requestDownProgressDelegate
-(void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength{
    [self.delegate zinkNet:self progressDownloadSize:newLength andReceivedSize:0 andIdentiffier:request.username];
    [_dicProgressInfo setObject:[NSString stringWithFormat:@"%lld", newLength] forKey:request.username];
}

-(void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes{
    long long allSize = [[_dicProgressInfo objectForKey:request.username] longLongValue];
    
    [self.delegate zinkNet:self progressDownloadSize:allSize andReceivedSize:bytes andIdentiffier:request.username];
}




@end

