//
//  ZinkNetManager.h
//  ZinkNetManager
//
//  Created by mac on 14-7-22.
//  Copyright (c) 2014年 林峥. All rights reserved.
//  1.0 版本
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"
#import "SHXMLParser.h"

#define ParserMethod    0   //0 表示使用XML解析， 1表示使用JSON解析
#define XMLParser       0
#define JSONParser      1

#define zinkServerDomain @""        //数据通讯地址

/**
 *测试url identiffier
 *约定:
 *1.返回值为字符串时,identiffier以str开头;
 *2.返回值为字典时,identiffier以dic开头;
 *3.返回值为数组时,identiffier以arr开头;
 *4.返回为文件时,identiffier以file开头;
 */
#define zinkIdTest @"strTest"


#define zinkHudTagOther 97000
#define zinkHudTagStop 97001

typedef NS_ENUM(NSInteger, ZErrorCode) {
    ZErrorCode_Cancel,
    ZErrorCode_NoNetwork
};

@protocol ZinkNetManagerDelegate;



@interface ZinkNetManager : NSObject<MBProgressHUDDelegate,  ASIProgressDelegate>{
    ASINetworkQueue * _queue;
    BOOL _isRequesting;             //是否正在请求网络      yes:是
}

@property(strong, nonatomic)id<ZinkNetManagerDelegate> delegate;
@property(strong, nonatomic)UIView * contentView;


+(id)shareNetworkManager;

//由于使用单例生成ZinkNetManager,在同一时间发出多次数据请求时,通过identiffier,来区分返回的数据

//以get方式请求数据   urlStr:目标url   identiffier:数据请求标识    isShow:是否显示数据下载进度
-(void)getWithUrl:(NSString * )urlStr andIdentiffier:(NSString *)identiffier andShowProgress:(BOOL)isShow;

//以post方式请求数据   urlStr:目标url   dicInfo:提交的数据   identiffier:数据请求标识   isShow:是否显示数据下载进度
-(void)postWithUrlstr:(NSString *)urlStr andDicInfo:(NSDictionary *)dicInfo andIdentiffier:(NSString *)identiffier andShowProgress:(BOOL)isShow;

//以post方式请求数据并上传文件   urlStr:目标url   dicInfo:提交的数据    filePath:被上传文件所在的路径    identiffier:数据请求标识
-(void)uploadFileWithUrl:(NSString *)urlStr andDicInfo:(NSDictionary *)dicInfo andFilePath:(NSString *)filePath andIdentiffier:(NSString *)identiffier;

//显示加载提示框   contentView:被添加载提示框的view    默认加载提示框显示的内容为取消加载。触摸加载提示框后，会停止所有网络请求
-(void)startHubWithView:(UIView *)contentView;
//显示加载提示框   contentView:被添加载提示框的view    strContent:加载提示框显示的内容
-(void)startHubWithView:(UIView *)contentView andContent:(NSString *)strContent;
//隐藏加载框
-(void)stopHub;
-(void)stopAllRequest;

//url编码
-(NSString *)urlEncode:(NSString *)string;

@end

@protocol ZinkNetManagerDelegate <NSObject>

@optional
//文本数据请求成功时
-(void)zinkNetFinish:(ZinkNetManager *)manager withResult:(id)result andIdentiffier:(NSString *)identiffier;
//文件数据请求成功时
-(void)zinkNetFinish:(ZinkNetManager *)manager withFile:(NSData *)result andIdentiffier:(NSString *)identiffier;
//数据下载进度
-(void)zinkNet:(ZinkNetManager *)manager progressDownloadSize:(long long)allSize andReceivedSize:(long long)receivedSize andIdentiffier:(NSString *)identiffier;
-(void)zinkNet:(ZinkNetManager *)manager failWithError:(NSError *)error;
@end
