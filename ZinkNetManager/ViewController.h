//
//  ViewController.h
//  ZinkNetManager
//
//  Created by mac on 14-7-22.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZinkNetManager.h"
#import "SHXMLParser.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController<ZinkNetManagerDelegate>

@end
