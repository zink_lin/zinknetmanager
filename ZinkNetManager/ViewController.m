//
//  ViewController.m
//  ZinkNetManager
//
//  Created by mac on 14-7-22.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){
    AVAudioPlayer * player;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    
    ZinkNetManager * netManager = [ZinkNetManager shareNetworkManager];
    
    NSString * strSong = [netManager urlEncode:@"小苹果"];
    NSString * strSinger = [netManager urlEncode:@"筷子兄弟"];
    NSString * strUrl = [NSString stringWithFormat:@"http://box.zhangmen.baidu.com/x?op=12&count=1&title=%@$$%@$$$$", strSong, strSinger];
    [[ZinkNetManager shareNetworkManager] stopAllRequest];
    [netManager getWithUrl:strUrl andIdentiffier:@"dicTest" andShowProgress:YES];
    
    netManager.delegate = self;
    [netManager startHubWithView:self.view];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
}

- (IBAction)actionTest:(id)sender {
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)zinkNetFinish:(ZinkNetManager *)manager withResult:(id)result andIdentiffier:(NSString *)identiffier{
    
    
    [manager getWithUrl:@"http://zhangmenshiting2.baidu.com/data2/music/1016279/1016279.mp3?xcode=a2b20961676d0a187863e9b220de574cdef9162e3637dd40&mid=0.42205796187351" andIdentiffier:@"fileTest" andShowProgress:YES];
}

-(void)zinkNetFinish:(ZinkNetManager *)manager withFile:(NSData *)result andIdentiffier:(NSString *)identiffier{
    NSLog(@"%@", @"complete");
    
    
    //目标路径
    NSString * docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * filePath = [docPath stringByAppendingPathComponent:@"test.mp3"];
    
    if ([result writeToFile:filePath atomically:YES]) {
        NSURL * urlMusic = [NSURL URLWithString:filePath];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:urlMusic error:nil];
        [player prepareToPlay];
        [player setVolume:1.0];
        [player play];
        
    }
    
    
    [[ZinkNetManager shareNetworkManager] stopHub];
}

-(void)zinkNet:(ZinkNetManager *)manager progressDownloadSize:(long long)allSize andReceivedSize:(long long)receivedSize andIdentiffier:(NSString *)identiffier{
    NSLog(@"%lld, %lld, %@", allSize, receivedSize, identiffier);
}
-(void)zinkNet:(ZinkNetManager *)manager failWithError:(NSError *)error{
    if (error.code == ZErrorCode_Cancel) {
        NSLog(@"取消");
    }
    NSLog(@"error:%@", error);
}

@end
